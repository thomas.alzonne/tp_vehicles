#include "vehicle.h"
#include <QVariant>
#include "vehiclefactory.h"


Vehicle::VehicleType Vehicle::type() const
{
    return m_type;
}

Vehicle::Vehicle(QString brand, QString model, qreal price, uint nbWheels, Vehicle::VehicleType type)
    :m_brand(brand)
    ,m_model(model)
    ,m_base_price(price)
    ,m_nbWheels(nbWheels)
    ,m_type(type)
{

}

Vehicle::Vehicle(Vehicle *vehicle)
    :m_brand(vehicle->brand())
    ,m_model(vehicle->model())
    ,m_base_price(vehicle->price())
    ,m_nbWheels(vehicle->nbWheels())
    ,m_type(vehicle->type())
{

}

int Vehicle::nbWheels()
{
    return m_nbWheels;
}

QString Vehicle::brand()
{
    return m_brand;
}

QString Vehicle::model()
{
    return m_model;
}

qreal Vehicle::price() const
{
    return m_base_price;
}

QString Vehicle::toString() {
    QString vehicleType = QVariant::fromValue(m_type).toString();
    return vehicleType + " - " + QString::number(nbWheels()) + " wheels - brand:" + brand() + " - model:" + model() +
        " - " + QString::number(price()) + "EUR";
}
