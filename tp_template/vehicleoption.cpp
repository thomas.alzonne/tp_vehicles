#include "vehicleoption.h"

VehicleOption::VehicleOption(Vehicle *vehicle, QString option, qreal price)
    :Vehicle(vehicle)
    ,m_vehicle(vehicle)
    ,m_optionName(option)
    ,m_optionPrice(price)
{

}

QString VehicleOption::toString() {
    return m_vehicle->toString() + ", " + m_optionName;
}

qreal VehicleOption::price() const
{
    return m_optionPrice + m_vehicle->price();
}
