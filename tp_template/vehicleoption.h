#ifndef VEHICLEOPTION_H
#define VEHICLEOPTION_H
#include <QString>
#include "vehicle.h"

class VehicleOption : public Vehicle {
    Vehicle *m_vehicle;
    QString  m_optionName  = "";
    qreal    m_optionPrice = 0;

public:
    VehicleOption(Vehicle *vehicle,QString option,qreal price);
    virtual QString toString() override;
    virtual qreal price() const override;
};


class VehicleOptionAbs : public VehicleOption {
  public:
    VehicleOptionAbs(Vehicle *vehicle) : VehicleOption(vehicle, "ABS", 1000) {
    }
};

class VehicleOption4X4 : public VehicleOption {
  public:
    VehicleOption4X4(Vehicle *vehicle) : VehicleOption(vehicle, "4X4", 5000) {
    }
};


class VehicleOptionSport : public VehicleOption {
  public:
    VehicleOptionSport(Vehicle *vehicle) : VehicleOption(vehicle, "Sport", 10000) {
    }
};


#endif // VEHICLEOPTION_H
