#include "vehicle.h"
#include <QDebug>
#include "vehiclefactory.h"
#include "vehicleoption.h"
#include <QObject>

uint comptagePoints(QString in, QString expect, uint points) {
    if (in == expect)
        return points;

    qDebug() << in << " / " << expect;
    return 0;
}

int main(int argc, char *argv[]) {
    // Q_UNUSED permet de supprimer les warnings des variables non utilisées
    Q_UNUSED(argc)
    Q_UNUSED(argv)
    const char *EXPECTS[] = {
        "MOTO - 2 wheels - brand:BMW - model:K1200R - 17000EUR",
        "CAR - 4 wheels - brand:Renault - model:Clio - 12000EUR",
        "MOTO - 2 wheels - brand:BMW - model:K1200R - 17000EUR, ABS",
        "MOTO - 2 wheels - brand:BMW - model:K1200R - 17000EUR, ABS, Sport",
        "CAR - 4 wheels - brand:Renault - model:Clio - 12000EUR, 4X4",
        "CAR - 4 wheels - brand:Renault - model:Clio - 12000EUR, 4X4, ABS",
        "MOTO - 2 wheels - brand:BMW - model:K1200R - 17000EUR, ABS, Sport - Price with options 28000EUR",
        "CAR - 4 wheels - brand:Renault - model:Clio - 12000EUR, 4X4, ABS - Price with options 18000EUR",
        "46000EUR"};

    uint nbPoints = 2;
    VehicleFactory *factory = VehicleFactory::getInstance();
    Vehicle *moto    = factory->create(Vehicle::MOTO, "BMW", "K1200R", 17000.0);
    Vehicle *car     = factory->create(Vehicle::CAR, "Renault", "Clio", 12000.0);
    nbPoints += comptagePoints(moto->toString(), EXPECTS[0], 2);
     qDebug() << moto->toString();
    nbPoints += comptagePoints(car->toString(), EXPECTS[1], 2);
     qDebug() << car->toString();

    moto = factory->updateOption(moto, new VehicleOptionAbs(moto));
    nbPoints += comptagePoints(moto->toString(), EXPECTS[2], 2);
     qDebug() << moto->toString();

    moto = factory->updateOption(moto, new VehicleOptionSport(moto));
    nbPoints += comptagePoints(moto->toString(), EXPECTS[3], 2);
     qDebug() << moto->toString();

    car = factory->updateOption(car, new VehicleOption4X4(car));
    nbPoints += comptagePoints(car->toString(), EXPECTS[4], 2);
     qDebug() << car->toString();

    car = factory->updateOption(car, new VehicleOptionAbs(car));
    nbPoints += comptagePoints(car->toString(), EXPECTS[5], 2);
     qDebug() << car->toString();


    // un qreal est un double
    qreal totalPrice = 0;
    uint  i          = 6;
    foreach (Vehicle *v, factory->getVehiclesList()) {
         qDebug() << v->toString() << " - Price with options" << v->price() << "EUR";
        QString strRes = v->toString() + " - " + "Price with options " + QString::number(v->price()) + "EUR";
        nbPoints += comptagePoints(strRes, EXPECTS[i++], 2);
        totalPrice += v->price();
    }

    nbPoints += comptagePoints(QString::number(totalPrice) + "EUR", EXPECTS[8], 2);
     qDebug() << "Total:" << totalPrice;


    qDebug() << "Votre note :" << nbPoints << "/ 20";
    return 0;
}
