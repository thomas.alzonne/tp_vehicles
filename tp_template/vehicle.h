#ifndef VEHICLE_H
#define VEHICLE_H
#include <QObject>
#include <QString>

class Vehicle : public QObject {
    Q_OBJECT

public:
    enum VehicleType { UNKNOWN, MOTO, CAR };
    Q_ENUM(VehicleType)

protected:
    QString     m_brand      = "";
    QString     m_model      = "";
    qreal       m_base_price = 0;
    uint        m_nbWheels   = 0;
    VehicleType m_type       = UNKNOWN;

public:
    Vehicle(QString brand, QString model,qreal price, uint nbWheels = 0, VehicleType type = VehicleType::UNKNOWN);
    Vehicle(Vehicle* vehicle);
    int nbWheels();
    virtual qreal price() const;
    QString brand();
    QString model();

    virtual QString toString();
    VehicleType type() const;
};

#endif // VEHICLE_H
