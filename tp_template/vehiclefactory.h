#ifndef VEHICLEFACTORY_H
#define VEHICLEFACTORY_H
#include "vehicle.h"
class VehicleFactory {
    static VehicleFactory *m_instance;
    VehicleFactory();
    QList<Vehicle *> vehiclesList;

public:
    static VehicleFactory *getInstance();
    Vehicle * create(Vehicle::VehicleType type, QString brand, QString model, qreal price);
    Vehicle * updateOption(Vehicle *target, Vehicle *option);
    QList<Vehicle *> getVehiclesList();

};


#endif // VEHICLEFACTORY_H
