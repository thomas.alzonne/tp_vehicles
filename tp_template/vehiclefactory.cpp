#include "vehiclefactory.h"

VehicleFactory *VehicleFactory::m_instance = nullptr;

VehicleFactory::VehicleFactory()
{

}

VehicleFactory *VehicleFactory::getInstance()
{
    if(m_instance == nullptr) {
        m_instance = new VehicleFactory();
    }
    return m_instance;
}

Vehicle *VehicleFactory::create(Vehicle::VehicleType type, QString brand, QString model, qreal price)
{
    Vehicle *vehicle;
    switch (type) {
    case Vehicle::Vehicle::MOTO:
        vehicle= new Vehicle(brand,model,price,2,type);
        break;
    case Vehicle::Vehicle::CAR:
        vehicle= new Vehicle(brand,model,price,4,type);
        break;
    default:
        vehicle= new Vehicle(brand,model,price,0,type);
    }
    vehiclesList.append(vehicle);
    return vehicle;
}

Vehicle *VehicleFactory::updateOption(Vehicle *target, Vehicle *option) {
    // qDebug() << __FUNCTION__ << target << option;
    int iTarget = vehiclesList.indexOf(target);
    if (iTarget != -1) {
        vehiclesList[iTarget] = option;
    }
    return option;
}

QList<Vehicle *> VehicleFactory::getVehiclesList()
{
    return vehiclesList;
}

